"use strict";

class Employee {
	constructor({name, age, salary}) {
        this.name = name;
		this.age = age;
		this.salary = salary;
    }
    set name(value) {
		this._name = value;
	}
	get name() {
		return this._name;
    }
    
	set age(value) {
		this._age = value;
	}
	get age() {
		return this._age;
    }
    
    set salary(value) {
		this._salary = value;
	}
	get salary() {
		return this._salary;
	}
}


class Programmer extends Employee {
	constructor({name, age, salary, lang}) {
		super ({name, age, salary});
		this.lang = lang;
    }
    set salary(value) {
		this._salary = value;
	}
	get salary() {
		return this._salary * 3;
	}
}

const junior = new Programmer ({
    name: "Alex", 
    age: 22, 
    salary: 3500, 
    lang: ['JS', 'C++', 'Pascal']
});
console.log("junior programmer: ", junior);

const middle = new Programmer ({
    name: "Zina", 
    age: 23, 
    salary:2500, 
    lang: ['JAVA', 'JS', 'PHP']
});
console.log("middle programmer: ", middle);

const senior = new Programmer ({
    name: "Ilya", 
    age: 55, 
    salary: 4100, 
    lang: ['Ruby', 'Python', 'C#', 'Swift', 'JS']
});
console.log("senior programmer: ", senior);